package appengine

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"google.golang.org/appengine/urlfetch"
)

/*Result for api call containting:
owner, top committer, commits, languages and project name
*/
type Result struct {
	Project   string   `json:"project"`
	Owner     string   `json:"owner"`
	Committer string   `json:"committer"`
	Commits   int      `json:"commits"`
	Language  []string `json:"language"`
}

func githubHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type", "application/json")
	suffix := strings.TrimPrefix(r.URL.Path, "/projectinfo/v1/github.com/")
	//checking for bad github link request
	suffixParts := strings.Split(suffix, "/")
	if len(suffixParts) != 2 {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	apiLink := "https://api.github.com/repos/" + suffix

	//Gets api data
	mainAPIDoc := linkToString(apiLink, r)
	languagesAPIDoc := linkToString(apiLink+"/languages", r)
	commitsAPIDoc := linkToString(apiLink+"/contributors", r)

	//Checks if all api doc's were found
	if mainAPIDoc == "" || languagesAPIDoc == "" || commitsAPIDoc == "" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	//vars for unmarshaled data
	var mainAPIObj, languagesAPIObj map[string]interface{}
	var commitsAPIObj []map[string]interface{}

	//parsing api data to mappings
	json.Unmarshal([]byte(mainAPIDoc), &mainAPIObj)
	json.Unmarshal([]byte(languagesAPIDoc), &languagesAPIObj)
	json.Unmarshal([]byte(commitsAPIDoc), &commitsAPIObj)
	//extracting data from mappings to result struct
	var result Result
	result.Project = suffixParts[1]
	result.Owner = mainAPIObj["owner"].(map[string]interface{})["login"].(string)
	result.Committer = commitsAPIObj[0]["login"].(string)
	result.Commits = int(commitsAPIObj[0]["contributions"].(float64))

	for i := range languagesAPIObj {
		result.Language = append(result.Language, i)
	}

	//fmt.Fprintln(w, languagesAPIObj)

	//Marshaling result struct to json result
	f, err := json.Marshal(result)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, string(f))
}

func linkToString(url string, r *http.Request) string {
	clientContext := r.Context()
	clientFetch := urlfetch.Client(clientContext)
	f, err := clientFetch.Get(url)
	if err != nil {
		return ""
	}
	str, err := ioutil.ReadAll(f.Body)
	if err != nil {
		return ""
	}
	return string(str)
}

func main() {

	http.HandleFunc("/projectinfo/v1/github.com/", githubHandler)
	http.ListenAndServe("127.0.0.1:8080", nil)
}
