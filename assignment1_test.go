package appengine

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

//Taken from https://github.com/marni/imt2681_studentdb/blob/master/api_student_test.go line 37
func Test_githubHandler_malformedURL(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(githubHandler))
	defer ts.Close()

	testCases := []string{
		ts.URL,
		ts.URL + "/projectinfo/v1/github.com/fsagdaas/agsgasfsa",
		ts.URL + "/projectinfo/v1/github.com",
	}
	for _, tstring := range testCases {
		resp, err := http.Get(tstring)
		if err != nil {
			t.Errorf("Error making the GET request, %s", err)
		}

		if resp.StatusCode != http.StatusBadRequest {
			t.Errorf("For route: %s, expected StatusCode %d, received %d", tstring,
				http.StatusBadRequest, resp.StatusCode)
			return
		}
	}
}
